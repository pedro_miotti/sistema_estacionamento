
document.getElementById('formulario').addEventListener('submit', cadastroVeiculo);

function cadastroVeiculo(e) {
    let modeloCarro = document.getElementById('modeloCarro').value;
    let placaCarro = document.getElementById('placaCarro').value;
    let time = new Date();
    //Geting the full hora e data e convertendo para string para o json
    let date_str = time.toString();
    
    
    if(!modeloCarro && !placaCarro){
      alert("Por favor, preencha todos os campos em branco !");
      return false;
    }

    carro = {
      modelo: modeloCarro,
      placa: placaCarro,
      hora: time.getHours(),
      minutos: time.getMinutes(),
      dataHora: date_str
    }

    if(localStorage.getItem('Patio') == null){
      let carros = [];
      //Colocando o objeto 'carro' no array 'carros'
      carros.push(carro);
      //Stringfy todo o array carros
      localStorage.setItem('Patio', JSON.stringify(carros));
      
    }
    
    else{

      let ls_carros = JSON.parse(localStorage.getItem('Patio'));

      for(let i = 0; i < ls_carros.length; i++){
        if(ls_carros[i].placa == placaCarro ){
            alert("Hello");
        }
      

      else{
        //Get todos o array do local storage
        // let ls_carros = JSON.parse(localStorage.getItem('Patio'));
        ls_carros.push(carro);
        //Stringfy todo o array carros
        localStorage.setItem('Patio', JSON.stringify(ls_carros));
      }
      }
    }
    //Apagando os inputs
    document.getElementById("formulario").reset();

    //Atualiza a lista do patio
    mostraPatio();

    //????
    e.preventDefault();
}


function apagarVeiculo(placa){
  let carros = JSON.parse(localStorage.getItem("Patio"));

  for(let i = 0; i < carros.length; i++){
    if(carros[i].placa == placa){
      //Remove do array
        carros.splice(i, 1);
    }
      localStorage.setItem("Patio", JSON.stringify(carros));
  }
  mostraPatio();

}

//CheckOut Box
function totalBox(modelo, placa, hora, minutos, dataHora){

  $("#popUpWin").modal({
    backdrop: 'static',
    keyboard: false
  });

  //When btt is clicked open the modal
  $('#popUpWin').modal('show');
  
  //Function to convert ms to time
  function msToTime(duration) {
    let prefix;
    let milliseconds = parseInt((duration % 1000) / 100),
      seconds = Math.floor((duration / 1000) % 60),
      minutes = Math.floor((duration / (1000 * 60)) % 60),
      hours = Math.floor((duration / (1000 * 60 * 60)) % 24);
  
    hours = (hours < 10) ? "0" + hours : hours;
    minutes = (minutes < 10) ? "0" + minutes : minutes;
    seconds = (seconds < 10) ? "0" + seconds : seconds;

    if (seconds > 0 && minutes == 0 && hours == 0) {
      prefix = " Sec";
    } else if (minutes > 0 && seconds != 0 && hours == 0) {
      prefix = " Min";
    } else if (hours > 0 && hours <= 24 && minutes != 1 && seconds >= 1) {
      prefix = " Horas";
    } else {
      prefix = " Dias";
  }
  
    return hours + ":" + minutes + ":" + seconds + " " + prefix;
  }
  

  let info_carro = document.getElementById('finalizaResults');

  info_carro.innerHTML = '';

  //Getting the data from the local Storage
  let carros = JSON.parse(localStorage.getItem('Patio'));
  
  const setTxt = document.getElementById("totalMin");

  for(let i = 0; i < carros.length; i++){
      //Pegando informacoes pela 'carros[].placa'
      if(carros[i].placa == placa){

        //Inserindo info selecionada na #TableId
        info_carro.innerHTML += '<tr><td>' + modelo + '</td><td>' + placa + '</td><td>' + hora + ':' + minutos + '</td>' + '</tr>';

        //Formating dataHora
        let date_now = new Date(dataHora);

        //Subtraindo as duas datas
        let diff = Math.abs(date_now - new Date());

        //Chamando funcao para converter 'diff' em minutos
        let hora_total = msToTime(diff);

        //Adding text to the Horatotal label
        setTxt.appendChild(document.createTextNode(hora_total)); 


      //Calculando o Total valor
      
      let valorTotal = 10;

      let minutosTotal  = Math.floor((diff / (1000 * 60)));
      
      if(minutosTotal >= 30){
        let totalMine = minutosTotal * 4 / 30;
        valorTotal += totalMine;
      }

      //Adding text to the total label
      const settotal = document.getElementById("total");
      settotal.appendChild(document.createTextNode("R$" + parseInt(valorTotal) + ",00"));

      //Deletar Veiculo quando finalizar e pressionado
      $('#btn_fin').on("click", function(){
        const delVec = apagarVeiculo(placa)
        $('#popUpWin').modal('hide');
        location.reload();
      });
        
        //Function to reload the page when the buttons 'cancel' and 'x' are clicked 
        $("#btt_cancel").on("click",function(){
          location.reload();
        });

        $("#xbtt").on("click",function(){
          location.reload();
        });       

        
      }
    }
  }


function mostraPatio() {
    //Pegando o conteudo dos carros adicionados no Local Storage
    let carros = JSON.parse(localStorage.getItem('Patio'));

    //Quando o Botao 'Adicionar' e pressionado adicion um <tr></tr> ao <tbody></tbody> 
    let carros_resultado = document.getElementById('resultados');

    carros_resultado.innerHTML = '';

    for(let i = 0; i < carros.length; i++){
        let modelo = carros[i].modelo;
        let placa = carros[i].placa;
        let hora = carros[i].hora;
        let minutos = carros[i].minutos;
        let dataHora = carros[i].dataHora;

        //Sera adicionado essa variavel ao <tbody></tbody>
        carros_resultado.innerHTML += '<tr><td>' + modelo + '</td><td>' + placa + '</td><td>' + hora + ':' + minutos + '</td><td>' +'<button class = "btn btn-danger" onClick="totalBox(\''+ modelo +'\',\''+ placa +'\',\''+ hora +'\', \''+ minutos +'\', \''+ dataHora +'\')">Finalizar</button>' + '</tr>';

    }  

}
